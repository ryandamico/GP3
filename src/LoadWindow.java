

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;


/**
 * Pirex load books window.
 */
public class LoadWindow extends JPanel
{

  /**
   * JPanel class for the Load Window.
   * 
   * @author Group K
   */
  private static final long serialVersionUID = 1L;

  private JPanel topTextFileBar, textFileType, titleAuthorBar, processBar, textPanelBar;
  private JTextField textFile, titleText, authorText;
  private JLabel textFileLabel, fileTypeLabel, titleLabel, authorLabel;
  private JButton browseButton, processButton;
  private JComboBox<String> fileTypeDropdown;
  private JTextPane loadStatePane;
  private String[] fileTypeStrings = {"Project Gutenberg File"};

  private String author, title, filePath;

  private StyledDocument doc;

  @SuppressWarnings("unused")
  private int totalPostings = 0;

  /**
   * Creates Load Window, is displayed in main function.
   * 
   * @param summary
   *          - to access the text field and update it.
   */
  public LoadWindow(SummaryWindow summary)
  {
    super();
    this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

    // Create our various text labels
    textFileLabel = new JLabel("Text File:");
    fileTypeLabel = new JLabel("Text File Type:");
    titleLabel = new JLabel("Title:");
    authorLabel = new JLabel("Author:");

    // Create our text boxes/fields
    textFile = new JTextField();
    textFile.setMaximumSize(new Dimension(Integer.MAX_VALUE, textFile.getPreferredSize().height));
    titleText = new JTextField();
    titleText.setMaximumSize(new Dimension(Integer.MAX_VALUE, titleText.getPreferredSize().height));
    authorText = new JTextField();
    authorText
        .setMaximumSize(new Dimension(Integer.MAX_VALUE, authorText.getPreferredSize().height));

    // Create our buttons
    browseButton = new JButton("Browse");
    processButton = new JButton("Process");
    processButton.setEnabled(false);

    // Create dropdown
    fileTypeDropdown = new JComboBox<String>(fileTypeStrings);
    topTextFileBar = new JPanel();
    topTextFileBar.setBorder(new EmptyBorder(5, 5, 5, 5));

    loadStatePane = new JTextPane();
    loadStatePane.setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, Color.GRAY));
    loadStatePane.setEditable(false);
    doc = loadStatePane.getStyledDocument();

    topTextFileBar.setLayout(new BoxLayout(topTextFileBar, BoxLayout.LINE_AXIS));
    topTextFileBar.add(textFileLabel);
    topTextFileBar.add(textFile);
    topTextFileBar.add(browseButton);

    textFileType = new JPanel();
    textFileType.setBorder(new EmptyBorder(5, 5, 5, 5));
    textFileType.setLayout(new BoxLayout(textFileType, BoxLayout.LINE_AXIS));
    textFileType.add(fileTypeLabel);
    textFileType.add(fileTypeDropdown);

    titleAuthorBar = new JPanel();
    titleAuthorBar.setBorder(new EmptyBorder(5, 5, 5, 5));
    titleAuthorBar.setLayout(new BoxLayout(titleAuthorBar, BoxLayout.LINE_AXIS));
    titleAuthorBar.add(titleLabel);
    titleAuthorBar.add(titleText);
    titleAuthorBar.add(authorLabel);
    titleAuthorBar.add(authorText);

    processBar = new JPanel();
    processBar.setBorder(new EmptyBorder(5, 5, 5, 5));
    processBar.setLayout(new BoxLayout(processBar, BoxLayout.X_AXIS));
    processBar.add(processButton);
    processBar.add(Box.createHorizontalGlue());

    textPanelBar = new JPanel();
    textPanelBar.setBorder(new EmptyBorder(5, 5, 5, 5));
    textPanelBar.setLayout(new BoxLayout(textPanelBar, BoxLayout.LINE_AXIS));
    textPanelBar.add(loadStatePane);

    this.add(topTextFileBar);
    this.add(textFileType);
    this.add(titleAuthorBar);
    this.add(new JSeparator(SwingConstants.HORIZONTAL));
    this.add(processBar);
    this.add(textPanelBar);

  }
}

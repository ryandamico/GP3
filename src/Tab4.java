

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextPane;

/**
 * JPanel extended class for the Pirex Summary Window.
 * 
 * @author Group K
 */
public class Tab4 extends JPanel
{
  public static final long serialVersionUID = 1L;
  protected JTextPane summaryForm;
  // private JPanel summaryPanel;

  /**
   * Constructs the Pirex Search Window for display in main().
   */
  public Tab4()
  {
    // call parent and set layout
    super();
    this.setLayout(new GridLayout());

    summaryForm = new JTextPane();
    summaryForm.setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, Color.GRAY));
    summaryForm.setSize(summaryForm.getPreferredSize());
    summaryForm.setEditable(false);
    summaryForm.setText("nortons");

    this.add(summaryForm);
  }
}
